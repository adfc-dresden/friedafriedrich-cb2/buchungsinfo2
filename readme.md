Buchungsinfo2 ist ein Wordpress-Plugin, das das Commonsbooking-Plugin (Version 2.x) um eine Ansicht für eine eingeschränkte Nutzergruppe erweitert. Abonnenten (subscriber) haben keinen Zugriff, aber bereits Mitarbeiter (contributor) haben Zugriff. Bei Frieda&Friedrich nennen wir den Nutzerkreis "Lastenrad-Paten". Das ist eine relativ große Gruppe und diese sind (im Gegensatz cb_manager) nur sehr einfach geschult. Die Ansicht wird mit dem Shortcode [buchungsinfo2] ausgegeben. Es ist angedacht, den Code auf einer sonst leeren Seite mit dem Namen "Buchungsinfo" zu platzieren. Es wird eine Liste mit den Buchungen der letzten x Tagen und den nächsten x Tagen ausgegeben und zwar nur für die Lastenrad-Standorte, die vom gegebenen Nutzer verwaltet werden. Das wird vom Admin unter dem jeweiligen Nutzer als Liste der verwaltenden Standorte (locations_managed) angegeben. Dafür ist das Plugin Advanced Custom Fields auch erforderlich. Angelegt muss eine ACF-Feldgruppe "cb" und darin das Feld "locations_managed":
- Feldtyp: Beitragsobjekt
- Inhaltstyp "Standort"
- Rückgabe "Beitragsobjekt"
- Select Multiple
- Feldgruppe anzeigen, wenn "Benutzerformular" ist gleich "alle" UND "Aktuelle Benutzerrolle" ist gleich "Administrator"

