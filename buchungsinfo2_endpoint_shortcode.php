<?php

/* fLottes custom-shortcodes-cb-bookings-overviews.php cb_bookings_location_shortcode nachempfunden */

/* buchungsinfo in cb1:
 * Plugin ACF mit Feldgruppe "cb" und darin Feld "locations_managed"
 * Feldtyp: Beitragsobjekt, Inhaltstyp "Standort", Rückgabe "Beitragsobjekt", Select Multiple
 * (Feldgruppe anzeigen, wenn "Benutzerformular" ist gleich "alle" UND "Aktuelle Benutzerrolle" ist gleich "Administrator")
 */
 
define("BUCHUNGSINFO2_FIELDNAME", "locations_managed");
define("BUCHUNGSINFO2_MAXDAYS", 15);

add_shortcode("buchungsinfo2", "buchungsinfo2_shortcode");


function buchungsinfo2_shortcode($atts)
{
    $html_output = '';

    $html_output .= '<h1>Buchungsinfo</h1>';

    if (!checkPermission($html_output)) return $html_output;

    $current_user = wp_get_current_user();
    $user_info = get_userdata($current_user->ID);
    $html_output .= "<p>Hallo ". $user_info->first_name . ",";
    
    $adminlevel = in_array( 'administrator', (array) $user_info->roles ) ||
                  in_array( 'super_cb_manager', (array) $user_info->roles );

    if ( $adminlevel ) {
        $html_output .= "<br>als Administrator bzw. Super CB Manager siehst du hier immer die Buchungsübersicht zu allen Standorten, sowie die vollständige Patenliste.</p>";
        renderUsertable($html_output);
    } else {
        $html_output .= "<br>du bist berechtigt, die Buchungsinfos für ausgewählte Verleihstationen einzusehen.</p>";
    }
    
    if ( $adminlevel ) {
        $locations = get_posts( array(
            'post_type'      => 'cb_location',
            'post_status'    => 'publish',
            'order'          => 'ASC',
            'posts_per_page' => -1
        ));

    } else {
        // contributors and cb_managers (edit_posts permission was already checked)
        $locations = get_field(BUCHUNGSINFO2_FIELDNAME, 'user_' . $current_user->ID);
    }

    if ( $locations ) {
        foreach ( $locations as $location ) {
            renderBookings($location->ID, $html_output);
        }
    }

    return $html_output;
}


function checkPermission(&$html_output) {
    // Stop if user is not a least contributor (Mitarbeiter) (contributors and higher can 'edit_posts')
    if (!is_user_logged_in()) {
        $redirect_url = esc_url($_SERVER['REQUEST_URI']);
        $html_output .= '<h3>Bitte einloggen</h3>';
        $html_output .= "<script>window.location.href = '" . wp_login_url($redirect_url) . "';</script>";
        return false;
    } else {
        if (!current_user_can('edit_posts')) {
            // User is logged in but cannot edit posts, display message
            $html_output .= '<h3>Zugang nur ab Mitarbeiter-Rolle (eng. contributor)</h3>';
            return false;
        }
    }

    // inject CSS to hide header (logo, menu, slogan) of webpage when rendering page with shortcode
    $html_output .= <<<EOD
    <script>;
    jQuery(document).ready(function ($) {
        $('header').hide();
        $('main').css('padding', '1em');
        });
    </script>
    EOD;

    // Stop if user is not a least contributor (Mitarbeiter) (contributors and higher can 'edit_posts')
    if (!current_user_can('edit_posts')) {
        $html_output .= '<h3>Zugang nur ab Mitarbeiter-Rolle (eng. contributor)</h3>';
        return false;
    }
    
    return true;
}

function renderUsertable(&$html_output) {
    $users = get_users(array(
        'role__not_in' => array('subscriber'),
        'meta_key'     => BUCHUNGSINFO2_FIELDNAME,
        'meta_value'   => '',
        'meta_compare' => '!='
    ));

    $headers = ["Wer",
                "Mail",
                "Telefon",
                "Standorte"
                ];

    $html_output .= '<table class="bookinglist"><thead><tr><th>' . implode('</th><th>', $headers) . '</th></tr></thead><tbody>';

    foreach ( $users as $user ) { 
            $user_locations = get_field(BUCHUNGSINFO2_FIELDNAME, 'user_'.$user->ID);

            if ( $user_locations) {
                $rowfields = [  $user->first_name . " " . $user->last_name,
                                $user->user_email,
                                $user->phone,
                                "<ul><li>" . implode('</li><li>', array_column($user_locations, 'post_title')) . "</li></ul>"
                             ];

                $html_output .= "<tr><td>" . implode("</td><td>", $rowfields) . "</td></tr>";
            }
    }
    $html_output .= "</tbody></table>";
}


function renderBookings($locationId, &$html_output) {

    $nowDateFormatted = current_time("j. n. Y", false);

    $loc_name = get_the_title($locationId);
    $html_output .= "<p><b>Buchungsübersicht für Standort: $loc_name</b><br>(Buchungsstand vom $nowDateFormatted für +/-" . BUCHUNGSINFO2_MAXDAYS . " Tage)</p>";

    $nowLocalTimestamp = current_time('timestamp', false);

    $booking_posts = get_posts(array('post_type' => 'cb_booking',
        'posts_per_page' => -1,
        'post_status' => array('publish', 'confirmed'),
        'order'          => 'ASC',
        'orderby'        => 'meta_value',
        'meta_key'       => 'repetition-start',
        'meta_query'     => array(
        'relation' => 'AND',
        array(
            'key'     => 'repetition-start',
            'value'   => $nowLocalTimestamp - BUCHUNGSINFO2_MAXDAYS * 24 * 60 * 60,
            'compare' => '>'
        ),
        array(
            'key'     => 'repetition-end',
            'value'   => $nowLocalTimestamp + BUCHUNGSINFO2_MAXDAYS * 24 * 60 * 60,
            'compare' => '<'
        ),
        array(
            'key'     => 'location-id',
            'value'   => $locationId,
            'compare' => '=='
        )
    )
    ));

    $item = '';
    $theCurrentOrUpcomingBookingWasMarked = false;

    foreach ( $booking_posts as $booking )
    {
        $thisItemId = get_post_meta($booking->ID, 'item-id', true);
        if ($thisItemId != $item) {
            $item = $thisItemId;
            $item_name = get_the_title($item);

            $headers = ["von",
                        "bis",
                        "gebucht von",
                        "Mail",
                        "Telefon",
                        "Code",
                        "Kommentar"
                        ];

            $html_output .= "<table class='bookinglist'>";
            $html_output .= "<thead>";
            $html_output .= "<tr><td colspan='" . count($headers) . "'><b>Lastenrad: <a href='" . get_permalink($item)  . "'>" . esc_html($item_name) . "</a></b></td></tr>";
            $html_output .= "<tr><th>" . implode('</th><th>', $headers) . "</th></tr>";
            $html_output .= "</thead><tbody>";
        }
        $user = get_user_by('id', $booking->post_author); 
        $unix_time_start = get_post_meta($booking->ID, 'repetition-start', true);
        $unix_time_end = get_post_meta($booking->ID, 'repetition-end', true);
        $start_date_time = new DateTime("@$unix_time_start");
        $start_date_time->setTimeZone(new DateTimeZone('GMT'));
        $end_date_time = new DateTime("@$unix_time_end");
        $end_date_time->setTimeZone(new DateTimeZone('GMT'));
        $bookingcode = get_post_meta($booking->ID, '_cb_bookingcode', true);
        $comment = get_post_meta($booking->ID, 'comment', true);
        
        // Mark the current or upcoming booking (ie. last booking not over yet)
        $style = '';
        if (!$theCurrentOrUpcomingBookingWasMarked && ($unix_time_end > $nowLocalTimestamp)) {
            $style = 'style="background-color: lightgreen;"';
            $theCurrentOrUpcomingBookingWasMarked = true;
        }

        $rowfields = [  $start_date_time->format('j. n.'),
                        $end_date_time->format('j. n.'),
                        esc_html($user->first_name . " " . $user->last_name),
                        esc_html($user->user_email),
                        esc_html($user->phone),
                        esc_html($bookingcode),
                        esc_html($comment) ];

        $html_output .= "<tr $style><td>" . implode("</td><td>", $rowfields) . "</td></tr>";

    }
    $html_output .= "<tbody></table>";

}